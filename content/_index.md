+++
title = "Candidatos"
sort_by = "weight"
+++

# O nosso programa

## Juntos com as pessoas

* Presença  dos membros eleitos nas ruas da  freguesia  juntamente com os funcionários da junta, para identificar problemas e procurar soluções com os  habitantes.
* Promover a  participação dos habitantes nas discussões sobre o futuro da  freguesia.
* Criação de um portal de sugestões, onde os habitantes podem sugerir melhorias para a qualidade de vida da freguesia.
* Abertura da junta em Vila Verde e Barbudo.

## Apoiar as pessoas

* Criar uma aplicação de requisição de documentos online, com o objetivo de permitir aos residentes pedirem em qualquer altura documentos como por exemplo "Comprovativo de residência".
* Criar uma aplicação que permita aos habitantes comunicarem de forma rápida problemas existentes na sua rua. (Exemplo: Sinal de trânsito em falta, fugas de água, falhas de iluminação, etc...)
* Criação de gabinete de apoio à ação social e associações da freguesia.
* Disponiblizar os meios da Junta no apoio a iniciativas de recolha de bens essenciais para as famílias mais carenciadas.

## Respeitar os feirantes

* Colocar casas de banho nas feiras quinzenais.
* Criar um número de emergência para os feirantes reportarem problemas de modo a serem resolvidos problemas prontamente. Como por exemplo: Resolver em tempo útil o problema de ter veículos estacionados no momento em que é necessário montar as tendas.
* Ajustar os horários dos fiscais às reias necessidades dos feirantes.
* Apoios monetários aos feirantes com as cotas em dia.

## Vila Verde e Barbudo, uma freguesia limpa

* Manutenção e limpeza de caminhos e vias da responsabilidade da junta.
* Criar um movimento de voluntariado com o objetivo de ajudar a preservar a limpeza da freguesia. Efetuando caminhadas e convívios com o objetivo de limpar algum local da freguesia.
* Pressão constante junto da Câmara para a instalação de ecopontos em locais com maior número de população.
* Dinamizar a feira com animação de rua, com objetivo de cativar mais clientes.

## Educação

* Apoio aos alunos e estabelecimentos do ensino pré -escolar e 1º ciclo.
* Criar ações de formação para jovens e adultos da freguesia, como por exemplo aulas de inglês.
* Acompanhamento das necessidades para alunos com necessidades educativas especiais.
* Reorganizar as atividades extracurriculares.
* Reforçar junto da Câmara a adaptação da casa do conhecimento às reais necessidades dos jovens que pretendem ingressar no ensino universitário. Ser um local onde os jovens possam fazer o ingresso ao ensino superior e obter informações de alojamento entre outras.

## Cultura e tempo livre em Vila Verde e Barbudo

* Criar passeios e convívios para todas as faixas etárias da freguesia.
* Renovação do espaço desportivo junto à igreja de vila verde, conhecido como “O Ringue”.
* Criar eventos mensais na casa da cultura com objetivo de promover os artistas locais como por exemplo teatro, concertos, fotografias, apresentações de livros entre outras.
* Promover encontros culturais na freguesia como encontro de concertinas, músicos locais entre outros.
* Dinamizar as atividades físicas, com aulas de zumba entre outras.
* Promover o turismo de natureza, com a criação de trilhos e geocaching.

## Juntos com os jovens

* Criar sunset’s em distintos locais da freguesia juntamente com os bares.
* Eventos culturais dedicados aos jovens como por exemplo, dj’s, bandas.
* Fazer parcerias com quintas, bares e restauração dinamizando eventos que aumentem a afluência dos jovens.
* Disponibilizar sala com projetor para apoiar os jovens nas suas atividades.
* Criar um workout park.

## Ressuscitar o comércio local

* Assegurar animação de verão de modo a tornar a freguesia um ponto de interesse.
* Estar mais próximo dos comerciantes em reuniões com o objetivo de potencializarmos o seu negócio.
* Criar um portal onde estejam todas as lojas do comércio local, permitindo as mesmas terem acesso a vendas online e divulgação da sua empresa.
* Promover ações de formação sobre evolução no setor de vendas.

## Construir a freguesia

* Reforçar juntamente com a Câmara a construção da casa mortuária na freguesia.
* Requalificação do cemitério em Barbudo.
* Melhoria dos caminhos rurais de serviço à população e explorações agrícolas.
* Reforçar junto da Câmara a necessidade de resolver o problema da escassez de água na rua da igreja velha. A água chega sem pressão.
* Juntamente com a Câmara melhorar os caminhos que não têm acesso a ambulâncias ou em que o acesso é bastante limitado.

## Cuidar dos idosos

* Estar atento aos idosos que se encontram isolados.
* Promover ações de vigilância na saúde.
* Disponibilizar para os idosos isolados um sistema que permita reportar situações S.O.S.
